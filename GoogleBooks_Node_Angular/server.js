var express = require('express');
var bodyParser = require('body-parser');
var http = require('https');
var path = require('path');
var app = express();
var request = require('request');
var public = path.join(__dirname, 'public');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.get('/', function(req, res) {
    res.sendFile(path.join(public, 'index.html'));
});

app.use('/images',express.static(path.join(__dirname, 'public/images')));
app.use('/script',express.static(path.join(__dirname, 'public/script')));
app.use('/css',express.static(path.join(__dirname, 'public/css')));

app.post('/getbooks', function(req, res) {
    res.header('Content-Type', 'application/json');

	var bookDetails = {};
	for(var key in req.body) {
		bookDetails = JSON.parse(key);
	}

	url = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&startIndex=1&q=' + bookDetails.bookTitle; + '&sort=' + bookDetails.sortType;;

	request(url, function (error, response, body) {
	  res.end(body);
	});
});

app.listen(8080, function() {
    console.log("Server listening @ http://127.0.0.1:8080/");
});