$(document).on("click", ".book-title", function() {
    $('.book-description').slideUp(200);
    $(this).parent().parent().find('.book-description').slideDown(300);
});

var app = angular.module('booksApp', []);
app.controller('booksController', function($scope, $http) {

      

    $scope.book = {};
    $scope.book.title = 'php';
    $scope.book.sortType = 'newest';
    


    $scope.bookList = {};

    $scope.serverBooksData = function() {
        if($scope.book.title == '') {
            $('.bookTitleTxt').focus();
            return false;
        }
		
		$('.loader').fadeIn(100);
        bookDetails = $scope.book;

        $scope.book.totalCount = 0;
        $scope.book.perPage = 10;
        $scope.book.pagination = [];
        $scope.book.fromRecord = 0;
        $scope.authorsList = {};
        $scope.maxAuthor = [];
        $scope.maxAuthorList = '';

		requestUrl = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&startIndex=1&q=' + bookDetails.title + '&sort=' + bookDetails.sortType;
        var options = {
            method: 'GET',
            url: requestUrl,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
        }
        
        var startTime = performance.now();
        var endTime = performance.now();
		$http(options).then(function(response){
            //console.log(response);
            $scope.bookList = response.data;
            $scope.book.totalCount = ($scope.bookList.items).length;
            endTime = performance.now();

            for(var book in $scope.bookList.items) {
                if($scope.bookList.items.hasOwnProperty(book)) {
                    for(var author in $scope.bookList.items[book].volumeInfo.authors) {
                        if($scope.bookList.items[book].volumeInfo.authors.hasOwnProperty(author)) {
                            var auth = $scope.bookList.items[book].volumeInfo.authors[author];
                            //console.log(auth);
                            if(auth) {
                                $scope.authorsList[auth] = (($scope.authorsList[auth]) ? ($scope.authorsList[auth]) : 0) + 1;
                                $scope.maxAuthor[$scope.authorsList[auth]] = (($scope.maxAuthor[$scope.authorsList[auth]]) ? ($scope.maxAuthor[$scope.authorsList[auth]] + ", ") : '') + auth;
                            }
                        }
                    }
                }
            }

            $scope.maxAuthorList = $scope.maxAuthor[$scope.maxAuthor.length - 1];
            
            $scope.book.performanceTime = ((Math.round(endTime - startTime) % 60000) / 1000).toFixed(2);

            let totalPage = Math.round($scope.book.totalCount / $scope.book.perPage);
            for(var i = 0; i < totalPage; i++) {
                $scope.book.pagination.push({pageNumber : i + 1, startRecord : i*$scope.book.perPage});
            }

            //console.log($scope.book.pagination);
			$('.loader').fadeOut(100);

        }, function(err){
            //console.log(err);
            endTime = performance.now(); 
            $scope.book.performanceTime = ((Math.round(endTime - startTime) % 60000) / 1000).toFixed(2);
			$('.loader').fadeOut(100);
			
        });
    };

    $scope.serverBooksData();

  
});
var pageLoadEndTime = performance.now(); 
document.querySelector('.pageLoadTime').innerHTML = ((Math.round(pageLoadEndTime - pageLoadStartTime) % 60000) / 1000).toFixed(2);	